/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.softwarefundamentals.pickacard;
import java.util.Scanner;
/**
 * A class that fills a magic hand of 7 cards with random Card Objects
 * and then asks the user to pick a card and searches the array of cards
 * for the match to the user's card. To be used as starting code in ICE 1
 * @author dancye
 */
public class CardTrick {
    
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        Card[] magicHand = new Card[7];
        for (int i=0; i<magicHand.length; i++)
        {
            Card c = new Card();
            System.out.print("any card: ");
            String card = sc.next(); 
            if(card.equals("Hearts"))
                System.out.println("You have chosen magical card");
            else if(card.equals("Diamonds"))
                System.out.println("You have chosen magical card");
            else if(card.equals("Spades"))
                System.out.println("You have chosen magical card");
            else if(card.equals("Clubs"))
                System.out.println("You have chosen magical card");
            else
                System.out.println("You have chosen simple card");
        }  
       
    }
    
}
